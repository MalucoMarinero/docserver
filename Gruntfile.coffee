'use strict'
fs = require 'fs'
path = require 'path'
mm = require 'minimatch'
wrench = require 'wrench'
{spawn} = require 'child_process'


module.exports = (grunt) ->
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

  grunt.initConfig {
    pkg: grunt.file.readJSON('package.json')
    coffee:
      client:
        options:
          join: true
          sourceMaps: true
        files:
          'build/share/assets/js/app.js': 'src/client/coffee/*.coffee'
      server:
        expand: true
        flatten: true
        cwd: 'src/server'
        src: ['*.coffee']
        dest: 'build/server/'
        ext: '.js'
    compass:
      client:
        options:
          sassDir: 'src/client/sass'
          cssDir: 'build/share/assets/css'
    hamlpy:
      client:
        src: 'src/client'
        dest: 'build/share'
    watch:
      general:
        files: [
          'src/server/*.coffee'
          'src/client/**/*.{coffee,haml,sass}'
        ]
        tasks: ['reloadDispatcher:general']
    reloadDispatcher:
      general:
        "**/*.haml" : ['hamlpy:client']
        "**/*.sass" : ['compass:client']
        "**/*.coffee" : ['coffee:client', 'coffee:server']
  }

  grunt.renameTask 'regarde', 'watch'

  grunt.registerMultiTask 'hamlpy', 'Compile HAML Files into HTML', () ->
    options = this.options()
    done = this.async()

    this.files.forEach (f) ->
      console.log "Compiling haml from #{ f.src } to #{ f.dest}"
      dest = f.dest
      dir = path.dirname dest

      f.src.map (srcPath) ->
        files = wrench.readdirSyncRecursive srcPath
        files = files.filter mm.filter "**/*.haml"

        files.forEach (srcRelPath) ->
          destRelPath = srcRelPath.replace '.haml', '.html'
          destRelDir = destRelPath.replace /[^\\\/?%*:|"<>"]+\.html/, ""
          if destRelDir
            if not fs.existsSync "#{ dest }/#{ destRelDir }"
              fs.mkdirSync "#{ dest }/#{ destRelDir }"
          process = spawn 'hamlpy', [
            "#{ srcPath }/#{ srcRelPath }"
            "#{ dest }/#{ destRelPath }"
          ]
          process.stdout.on 'data', (data) -> console.log data.toString()
          process.stderr.on 'data', (data) -> console.error data.toString()
          process.on 'exit', (code) ->
            if code is 0
              console.log "  '#{ srcPath }/#{ srcRelPath }' -> '#{ dest }/#{ destRelPath }'"

  grunt.registerMultiTask 'reloadDispatcher', 'Run tasks based on extensions.', ->
    for pattern, tasks of this.data
      if grunt.regarde.changed.some mm.filter pattern
        grunt.task.run tasks

  grunt.registerTask 'develop', ->
    grunt.task.run [
      'watch:general'
    ]

