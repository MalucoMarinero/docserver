'use strict'

http = require 'http'
https = require 'https'
fs = require 'fs'
wrench = require 'wrench'
AdmZip = require 'adm-zip'
targz = require 'tar.gz'
ncp = require 'ncp'
{print} = require 'sys'
{spawn} = require 'child_process'

registry = require './registry.js'

fetchState =
  working: false
  stack: []

exports.getFetchState = (cb) ->
  cb fetchState


assessSrcPath = (srcPath) ->
  location = ''
  retrieveAs = ''
  if srcPath.match /^(http|https|ftp)/
    location = 'remote'
  else
    location = 'local'
  if srcPath.match /\.(zip|tar\.gz)$/
    retrieveAs = 'zip'
  else if srcPath.match /\.(html)/
    retrieveAs = 'html'
  else
    retrieveAs = 'directory'
  return "#{ location } #{ retrieveAs }"

exports.fetchFile = (docRoot, opt, cb) ->
  if not fs.existsSync "#{ docRoot }/#{ opt.lang }/#{ opt.lib }/#{ opt.version }"
    wrench.mkdirSyncRecursive "#{ docRoot }/#{ opt.lang }/#{ opt.lib }/#{ opt.version }"

  targetDir = "#{ docRoot }/#{ opt.lang }/#{ opt.lib }/#{ opt.version }"

  fetchObj =
    lang: opt.lang
    lib: opt.lib
    version: opt.version
    src: opt.src
    status: 'starting'
    progress: 0
    progressEnd: 0

  fetchState.stack.push fetchObj
  fetchState.working = true
  
  handler = null
  assessment = assessSrcPath fetchObj.src

  switch assessment
    when "remote zip" then handler = remoteZip
    when "remote directory" then handler = remoteDirectory
    when "local directory" then handler = localDirectory

  if handler
    handler docRoot, targetDir, fetchObj
  else
    throw "Invalid Source Error, probably not supported yet."


  cb fetchState


remoteZip = (docRoot, targetDir, fetchObj) ->
  if fetchObj.src.match /^https:\/\//
    protocol = https
  else if fetchObj.src.match /^http:\/\//
    protocol = http
  else
    throw "Invalid Download Protocol"

  downloadFilename = fetchObj.src.match(/\/[\w\d.-]+$/)[0]
  fileName = "#{ docRoot }/#{ fetchObj.lang }/#{ fetchObj.lib }#{ downloadFilename }"
  file = fs.createWriteStream fileName
  protocol.get fetchObj.src, (res) ->
    fetchObj.status = 'downloading'
    fetchObj.progressEnd = parseInt res.headers['content-length'], 10

    res.pipe file
    res.on 'data', (chunk) ->
      fetchObj.progress += chunk.length
    res.on 'end', ->
      decompressDocs docRoot, fileName, targetDir, fetchObj


localZip = (docRoot, targetDir, fetchObj) ->
  decompressDocs docRoot, fetchObj.src, targetDir, fetchObj


remoteDirectory = (docRoot, targetDir, fetchObj) ->
  process = spawn 'wget', ['-mk', '-P', "#{targetDir}/", '--no-parent', fetchObj.src]
  fetchObj.status = 'downloading'
  fetchObj.progress = 50
  fetchObj.progressEnd = 100
  process.stderr.on 'data', (data) ->
    print data.toString()
  process.stdout.on 'data', (data) ->
    print data.toString()
  process.on 'exit', (code) ->
    fetchObj.status = 'registering'
    registry.registerDoc docRoot, fetchObj

    finishJob fetchObj

localDirectory = (docRoot, targetDir, fetchObj) ->
  fetchObj.status = 'copying'
  fetchObj.progress = 50
  fetchObj.progressEnd = 100
  ncp fetchObj.src, targetDir, {clobber: true}, (err) ->
    if err
      console.log err
    fetchObj.status = 'registering'
    registry.registerDoc docRoot, fetchObj

    finishJob fetchObj




decompressDocs = (docRoot, file, targetDir, fetchObj) ->
  fetchObj.status = 'decompressing'

  if file.match /\.zip$/
    zip = new AdmZip file
    zip.extractAllTo targetDir, true

    fetchObj.status = 'registering'
    registry.registerDoc docRoot, fetchObj

    finishJob fetchObj
  else if file.match /\.tar\.gz$/
    extract = new targz().extract file, targetDir, (err) ->
      if err
        console.log err
      fetchObj.status = 'registering'
      registry.registerDoc docRoot, fetchObj
      finishJob fetchObj

  

finishJob = (fetchObj) ->
  index = fetchState.stack.indexOf fetchObj
  if index != -1
    fetchState.stack.splice index, 1
  if fetchState.stack.length < 1
    fetchState.working = false
