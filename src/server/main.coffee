'use strict' 
connect = require 'connect'
http = require 'http'
fs = require 'fs'
url = require 'url'
path = require 'path'
commander = require 'commander'
router = require('flask-router')()

fetch = require './fetch.js'
registry = require './registry.js'

if not fs.existsSync "#{ process.env.HOME }/.node-docserver"
  fs.mkdirSync "#{ process.env.HOME }/.node-docserver"

docRoot = fs.realpathSync "#{ process.env.HOME }/.node-docserver"
shareRoot = fs.realpathSync "#{ __dirname }/../share"

registry.init docRoot

router.all '/docs/<path:path>', (req, res, next) ->
  req.url = req.params[0]
  req.path = req.params[0]
  connect.static(docRoot) req, res, ->
    res.end()

router.post '/api/start_fetch', (req, res, next) ->
  fetch.fetchFile docRoot, req.body, (state) ->
    res.write JSON.stringify(state)
    res.end()


router.get '/api/work_state', (req, res, next) ->
  fetch.getFetchState (state) ->
    res.write JSON.stringify(state)
    res.end()

router.post '/api/delete_doc', (req, res, next) ->
  registry.deleteDoc docRoot, req.body.lang, req.body.lib, req.body.version
  registry.getState (state) ->
    res.write JSON.stringify(state)
    res.end()

router.get '/api/registry_state', (req, res, next) ->
  registry.getState (state) ->
    res.write JSON.stringify(state)
    res.end()

router.all /.*/, (req, res, next) ->
  connect.static(shareRoot) req, res, ->
    res.end()


app = connect()
  .use(connect.favicon())
  .use(connect.bodyParser())
  .use(connect.logger('dev'))
  .use(router.route)


http.createServer(app).listen(8089, 'localhost')
