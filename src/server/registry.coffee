'use strict'

fs = require 'fs'
wrench = require 'wrench'
mm = require 'minimatch'

docRegistry =
  langs: []

exports.init = (docRoot) ->
  registryFilename = "#{ docRoot }/docRegistry.json"

  if fs.existsSync registryFilename
    string = fs.readFileSync registryFilename, encoding: 'utf-8'
    docRegistry = JSON.parse(string)
    console.log "Finished loading Document Registry"
  else
    string = JSON.stringify docRegistry
    fs.writeFileSync registryFilename, string
    console.log "Initialised Document Registry at #{ registryFilename }"


exports.getState = (cb) ->
  cb docRegistry


findIndexRoot = (docRoot, rootPath) ->
  list = wrench.readdirSyncRecursive "#{docRoot}/#{rootPath}"
  indexes = list.filter mm.filter("**/index.html")
  if indexes.length > 0
    return "#{ rootPath }/#{ indexes[0] }"
  else
    throw "index.html not found"


exports.registerDoc = (docRoot, opt) ->
  indexRoot = findIndexRoot docRoot, "#{ opt.lang }/#{ opt.lib }/#{ opt.version }"

  langFilter = docRegistry.langs.filter (l) -> l.name == opt.lang
  if langFilter.length
    langObj = langFilter[0]
  else
    langObj =
      name: opt.lang
      libs: []
    docRegistry.langs.push langObj

  libFilter = langObj.libs.filter (l) -> l.name == opt.lib
  if libFilter.length
    libObj = libFilter[0]
  else
    libObj =
      name: opt.lib
      versions: []
    langObj.libs.push libObj

  verFilter = libObj.versions.filter (l) -> l.name == opt.version
  if verFilter.length
    versionObj = verFilter[0]
  else
    versionObj =
      name: opt.version
    libObj.versions.push versionObj
  versionObj.root = indexRoot

  saveRegistry docRoot


exports.deleteDoc = (docRoot, lang, lib, version) ->
  if not lang
    saveRegistry docRoot
    return

  langFilter = docRegistry.langs.filter (l) -> l.name == lang
  langObj = langFilter[0]
  if not lib
    wrench.rmdirSyncRecursive "#{ docRoot }/#{ lang }", true
    docRegistry.langs.splice docRegistry.langs.indexOf(langObj), 1
    saveRegistry docRoot
    return

  libFilter = langObj.libs.filter (l) -> l.name == lib
  libObj = libFilter[0]
  if not version
    wrench.rmdirSyncRecursive "#{ docRoot }/#{ lang }/#{ lib }", true
    langObj.libs.splice langObj.libs.indexOf(libObj), 1
    saveRegistry docRoot
    return

  versionFilter = libObj.versions.filter (l) -> l.name == version
  versionObj = versionFilter[0]

  wrench.rmdirSyncRecursive "#{ docRoot }/#{ lang }/#{ lib }/#{ version }", true
  libObj.versions.splice libObj.versions.indexOf(versionObj), 1
  saveRegistry docRoot
  return


saveRegistry = (docRoot) ->
  registryFilename = "#{ docRoot }/docRegistry.json"

  string = JSON.stringify docRegistry
  fs.writeFileSync registryFilename, string
  console.log "Updated Document Registry - #{ registryFilename }"

  






