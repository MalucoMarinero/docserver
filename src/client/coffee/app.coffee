app = angular.module 'docserver', []
app.config ['$routeProvider', ($routeProvider) ->
  $routeProvider.
    when('/', {
      templateUrl: 'partials/home.html'
      controller: 'homeCtrl'
    }).
    when('/read/*readPath', {
      templateUrl: 'partials/read.html'
      controller: 'readCtrl'
    }).
    otherwise redirectTo: '/'
]

homeCtrl = ($scope, api) ->
  $scope.deleteDocs = (lang, lib, version) ->
    console.log 'deleting'
    api.deleteDoc lang, lib, version

homeCtrl.$inject = ['$scope', 'api']
app.controller 'homeCtrl', homeCtrl


readCtrl = ($scope, $route, $routeParams, $location) ->
  lastRoute = $route.current

  $scope.$on '$locationChangeSuccess', (event) ->
    if $route.current.$$route.controller == 'readCtrl'
      if $location.forceRouteChange
        delete $location.forceRouteChange
      else
        $route.current = lastRoute

  document.docPageChange = ->
    if this.activeElement.contentWindow
      match = this.activeElement.contentWindow.location.href.match /\/docs\/(.+)$/
      if match
        docsActiveURL = match[1]
        $location.url "/read/#{ docsActiveURL }"
        $scope.$apply()

  $scope.pathToLoad = "/docs/#{ $routeParams.readPath }"

readCtrl.$inject = ['$scope', '$route', '$routeParams', '$location']
app.controller 'readCtrl', readCtrl



docServerBar = ($scope, $route, $routeParams, $location, api) ->
  $scope.location = $location

  $scope.$on '$locationChangeSuccess', (event) ->
    params = $location.path().split '/'
    $scope.current =
      lang: params[2]
      lib: params[3]
      version: params[4]

  $scope.toggleAddingDoc = ->
    if $scope.newDoc
      delete $scope.newDoc
    else
      $scope.newDoc = 
        lang: ''
        lib: ''
        version: ''
        src: ''

  $scope.assumption = (srcPath) ->
    if not srcPath
      return ''
    location = ''
    retrieveAs = ''
    if srcPath.match /^(http|https|ftp)/
      location = 'remote download of'
    else
      location = 'local transfer of'
    if srcPath.match /\.(zip|tar\.gz|7z)$/
      retrieveAs = 'zipped file'
    else if srcPath.match /\.(html)/
      retrieveAs = 'single html file'
    else
      retrieveAs = 'directory'
    if retrieveAs == 'single html file'
      return "NOT YET SUPPORTED *** #{ location } a #{ retrieveAs }"
    if retrieveAs == 'zipped file' and location == 'local transfer of'
      return "NOT YET SUPPORTED *** #{ location } a #{ retrieveAs }"
    return "#{ location } a #{ retrieveAs }"

  $scope.getAltLangs = (currentLangName) ->
    if currentLangName
      langs = $.grep $scope.docRegistry.langs, (l) -> l.name != currentLangName
      return langs
    else
      return []

  $scope.getAltLibs = (langName, currentLibName) ->
    if langName and currentLibName
      [lang] = $.grep $scope.docRegistry.langs, (l) -> l.name == langName
      if lang
        libs = $.grep lang.libs, (l) -> l.name != currentLibName
        return libs
      else
        return []
    else
      return []

  $scope.getAltVersions = (langName, libName, currentVersionName) ->
    if langName and libName and currentVersionName
      [lang] = $.grep $scope.docRegistry.langs, (l) -> l.name == langName
      if lang
        [lib] = $.grep lang.libs, (l) -> l.name == libName
        if lib
          versions = $.grep lib.versions, (l) -> l.name != currentVersionName
          return versions
        else
          return []
      else
        return []
    else
      return []

  $scope.changeReadURL = (newURL) ->
    $location.forceRouteChange = true
    $location.path(newURL)
      

  $scope.startDownloading = ->
    api.startFetch $scope.newDoc

docServerBar.$inject = ['$scope', '$route', '$routeParams', '$location', 'api']
app.controller 'docServerBar', docServerBar


app.directive 'progressBar', ->
  return {
    restrict: "EAC"
    link: ($scope, elem, attrs) ->
      updateBar = ->
        if attrs.limit and attrs.progress
          decimal = $scope.$eval(attrs.progress) / $scope.$eval(attrs.limit)
          if decimal == NaN
            decimal = 0.0
        else
          decimal = 0.0
        if $(elem).find('.progress').length < 1
          $(elem).append("<div class='progress'></div>")
        decimal = $scope.$eval(attrs.progress ) / $scope.$eval(attrs.limit)
        totalWidth = $(elem).width()
        $(elem).find('.progress').animate({width: totalWidth * decimal}, 1000)
        $scope.percentage = decimal * 100 

      $scope.$watch attrs.progress, updateBar
      $scope.$watch attrs.limit, updateBar

      updateBar()

  }


app.filter 'sortVersions', ->
  return (versions) ->
    if versions
      return versions.sort (a, b) ->
        if a.name == b.name then return 0
        if a.name == 'latest' and b.name != 'latest' then return -1
        if a.name != 'latest' and b.name == 'latest' then return 1
        if a.name == 'current' and b.name != 'current' then return -1
        if a.name != 'current' and b.name == 'current' then return 1
        if a.name > b.name then return -1
        if a.name < b.name then return 1
        return 0
    return versions

app.factory 'api', ['$http', '$rootScope', '$timeout', ($http, $rootScope, $timeout) ->
  fetchState =
    working: false
    stack: []
  docRegistry =
    langs: []


  $rootScope.fetchState = fetchState
  $rootScope.docRegistry = docRegistry

  concatStackId = (item) ->
    return item.lang + item.lib + item.version

  updateFetchState = (newData) ->
    toRemove = []
    $.each fetchState.stack, (index, localItem) ->
      localItem.updated = false
      match = $.grep newData.stack, (serverItem, index2) ->
        concatStackId(serverItem) == concatStackId(localItem)
      if match.length == 1
        angular.extend localItem, match[0]
        match[0].pushed = true
        newData.stack.splice newData.stack.indexOf(match[0]), 1
      else
        toRemove.push index
    for index in toRemove
      fetchState.stack.splice index, 1
    newJobs = $.grep newData.stack, (serverItem, index2) ->
      if serverItem.pushed then false else true
    fetchState.stack = fetchState.stack.concat newJobs

    fetchState.working = newData.working


  apiObj =
    pollRegistry: ->
      req = $http.get '/api/registry_state'
      req.success (data, status, headers) ->
        angular.extend docRegistry, data
    startFetch: (newDoc) ->
      req = $http.post '/api/start_fetch', {
        lang: newDoc.lang
        lib: newDoc.lib
        version: newDoc.version
        src: newDoc.src
      }
      req.success (data, status, headers) ->
        updateFetchState data
        apiObj.pollFetchState()
    deleteDoc: (lang, lib, version) ->
      postData = {}
      if lang then postData.lang = lang
      if lib then postData.lib = lib
      if version then postData.version = version

      req = $http.post '/api/delete_doc', postData
      req.success (data, status, headers) ->
        angular.extend docRegistry, data
    pollFetchState: ->
      req = $http.get '/api/work_state'
      req.success (data, status, headers) ->
        if fetchState.stack.length != data.stack.length
          apiObj.pollRegistry()
        updateFetchState data
        if fetchState.working
          $timeout apiObj.pollFetchState, 1000
  apiObj.pollFetchState()
  apiObj.pollRegistry()
  return apiObj
]
