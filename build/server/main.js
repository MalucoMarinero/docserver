(function() {
  'use strict';
  var app, commander, connect, docRoot, fetch, fs, http, path, registry, router, shareRoot, url;

  connect = require('connect');

  http = require('http');

  fs = require('fs');

  url = require('url');

  path = require('path');

  commander = require('commander');

  router = require('flask-router')();

  fetch = require('./fetch.js');

  registry = require('./registry.js');

  if (!fs.existsSync("" + process.env.HOME + "/.node-docserver")) {
    fs.mkdirSync("" + process.env.HOME + "/.node-docserver");
  }

  docRoot = fs.realpathSync("" + process.env.HOME + "/.node-docserver");

  shareRoot = fs.realpathSync("" + __dirname + "/../share");

  registry.init(docRoot);

  router.all('/docs/<path:path>', function(req, res, next) {
    req.url = req.params[0];
    req.path = req.params[0];
    return connect["static"](docRoot)(req, res, function() {
      return res.end();
    });
  });

  router.post('/api/start_fetch', function(req, res, next) {
    return fetch.fetchFile(docRoot, req.body, function(state) {
      res.write(JSON.stringify(state));
      return res.end();
    });
  });

  router.get('/api/work_state', function(req, res, next) {
    return fetch.getFetchState(function(state) {
      res.write(JSON.stringify(state));
      return res.end();
    });
  });

  router.post('/api/delete_doc', function(req, res, next) {
    registry.deleteDoc(docRoot, req.body.lang, req.body.lib, req.body.version);
    return registry.getState(function(state) {
      res.write(JSON.stringify(state));
      return res.end();
    });
  });

  router.get('/api/registry_state', function(req, res, next) {
    return registry.getState(function(state) {
      res.write(JSON.stringify(state));
      return res.end();
    });
  });

  router.all(/.*/, function(req, res, next) {
    return connect["static"](shareRoot)(req, res, function() {
      return res.end();
    });
  });

  app = connect().use(connect.favicon()).use(connect.bodyParser()).use(connect.logger('dev')).use(router.route);

  http.createServer(app).listen(8089, 'localhost');

}).call(this);
