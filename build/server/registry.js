(function() {
  'use strict';
  var docRegistry, findIndexRoot, fs, mm, saveRegistry, wrench;

  fs = require('fs');

  wrench = require('wrench');

  mm = require('minimatch');

  docRegistry = {
    langs: []
  };

  exports.init = function(docRoot) {
    var registryFilename, string;

    registryFilename = "" + docRoot + "/docRegistry.json";
    if (fs.existsSync(registryFilename)) {
      string = fs.readFileSync(registryFilename, {
        encoding: 'utf-8'
      });
      docRegistry = JSON.parse(string);
      return console.log("Finished loading Document Registry");
    } else {
      string = JSON.stringify(docRegistry);
      fs.writeFileSync(registryFilename, string);
      return console.log("Initialised Document Registry at " + registryFilename);
    }
  };

  exports.getState = function(cb) {
    return cb(docRegistry);
  };

  findIndexRoot = function(docRoot, rootPath) {
    var indexes, list;

    list = wrench.readdirSyncRecursive("" + docRoot + "/" + rootPath);
    indexes = list.filter(mm.filter("**/index.html"));
    if (indexes.length > 0) {
      return "" + rootPath + "/" + indexes[0];
    } else {
      throw "index.html not found";
    }
  };

  exports.registerDoc = function(docRoot, opt) {
    var indexRoot, langFilter, langObj, libFilter, libObj, verFilter, versionObj;

    indexRoot = findIndexRoot(docRoot, "" + opt.lang + "/" + opt.lib + "/" + opt.version);
    langFilter = docRegistry.langs.filter(function(l) {
      return l.name === opt.lang;
    });
    if (langFilter.length) {
      langObj = langFilter[0];
    } else {
      langObj = {
        name: opt.lang,
        libs: []
      };
      docRegistry.langs.push(langObj);
    }
    libFilter = langObj.libs.filter(function(l) {
      return l.name === opt.lib;
    });
    if (libFilter.length) {
      libObj = libFilter[0];
    } else {
      libObj = {
        name: opt.lib,
        versions: []
      };
      langObj.libs.push(libObj);
    }
    verFilter = libObj.versions.filter(function(l) {
      return l.name === opt.version;
    });
    if (verFilter.length) {
      versionObj = verFilter[0];
    } else {
      versionObj = {
        name: opt.version
      };
      libObj.versions.push(versionObj);
    }
    versionObj.root = indexRoot;
    return saveRegistry(docRoot);
  };

  exports.deleteDoc = function(docRoot, lang, lib, version) {
    var langFilter, langObj, libFilter, libObj, versionFilter, versionObj;

    if (!lang) {
      saveRegistry(docRoot);
      return;
    }
    langFilter = docRegistry.langs.filter(function(l) {
      return l.name === lang;
    });
    langObj = langFilter[0];
    if (!lib) {
      wrench.rmdirSyncRecursive("" + docRoot + "/" + lang, true);
      docRegistry.langs.splice(docRegistry.langs.indexOf(langObj), 1);
      saveRegistry(docRoot);
      return;
    }
    libFilter = langObj.libs.filter(function(l) {
      return l.name === lib;
    });
    libObj = libFilter[0];
    if (!version) {
      wrench.rmdirSyncRecursive("" + docRoot + "/" + lang + "/" + lib, true);
      langObj.libs.splice(langObj.libs.indexOf(libObj), 1);
      saveRegistry(docRoot);
      return;
    }
    versionFilter = libObj.versions.filter(function(l) {
      return l.name === version;
    });
    versionObj = versionFilter[0];
    wrench.rmdirSyncRecursive("" + docRoot + "/" + lang + "/" + lib + "/" + version, true);
    libObj.versions.splice(libObj.versions.indexOf(versionObj), 1);
    saveRegistry(docRoot);
  };

  saveRegistry = function(docRoot) {
    var registryFilename, string;

    registryFilename = "" + docRoot + "/docRegistry.json";
    string = JSON.stringify(docRegistry);
    fs.writeFileSync(registryFilename, string);
    return console.log("Updated Document Registry - " + registryFilename);
  };

}).call(this);
