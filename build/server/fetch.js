(function() {
  'use strict';
  var AdmZip, assessSrcPath, decompressDocs, fetchState, finishJob, fs, http, https, localDirectory, localZip, ncp, print, registry, remoteDirectory, remoteZip, spawn, targz, wrench;

  http = require('http');

  https = require('https');

  fs = require('fs');

  wrench = require('wrench');

  AdmZip = require('adm-zip');

  targz = require('tar.gz');

  ncp = require('ncp');

  print = require('sys').print;

  spawn = require('child_process').spawn;

  registry = require('./registry.js');

  fetchState = {
    working: false,
    stack: []
  };

  exports.getFetchState = function(cb) {
    return cb(fetchState);
  };

  assessSrcPath = function(srcPath) {
    var location, retrieveAs;

    location = '';
    retrieveAs = '';
    if (srcPath.match(/^(http|https|ftp)/)) {
      location = 'remote';
    } else {
      location = 'local';
    }
    if (srcPath.match(/\.(zip|tar\.gz)$/)) {
      retrieveAs = 'zip';
    } else if (srcPath.match(/\.(html)/)) {
      retrieveAs = 'html';
    } else {
      retrieveAs = 'directory';
    }
    return "" + location + " " + retrieveAs;
  };

  exports.fetchFile = function(docRoot, opt, cb) {
    var assessment, fetchObj, handler, targetDir;

    if (!fs.existsSync("" + docRoot + "/" + opt.lang + "/" + opt.lib + "/" + opt.version)) {
      wrench.mkdirSyncRecursive("" + docRoot + "/" + opt.lang + "/" + opt.lib + "/" + opt.version);
    }
    targetDir = "" + docRoot + "/" + opt.lang + "/" + opt.lib + "/" + opt.version;
    fetchObj = {
      lang: opt.lang,
      lib: opt.lib,
      version: opt.version,
      src: opt.src,
      status: 'starting',
      progress: 0,
      progressEnd: 0
    };
    fetchState.stack.push(fetchObj);
    fetchState.working = true;
    handler = null;
    assessment = assessSrcPath(fetchObj.src);
    switch (assessment) {
      case "remote zip":
        handler = remoteZip;
        break;
      case "remote directory":
        handler = remoteDirectory;
        break;
      case "local directory":
        handler = localDirectory;
    }
    if (handler) {
      handler(docRoot, targetDir, fetchObj);
    } else {
      throw "Invalid Source Error, probably not supported yet.";
    }
    return cb(fetchState);
  };

  remoteZip = function(docRoot, targetDir, fetchObj) {
    var downloadFilename, file, fileName, protocol;

    if (fetchObj.src.match(/^https:\/\//)) {
      protocol = https;
    } else if (fetchObj.src.match(/^http:\/\//)) {
      protocol = http;
    } else {
      throw "Invalid Download Protocol";
    }
    downloadFilename = fetchObj.src.match(/\/[\w\d.-]+$/)[0];
    fileName = "" + docRoot + "/" + fetchObj.lang + "/" + fetchObj.lib + downloadFilename;
    file = fs.createWriteStream(fileName);
    return protocol.get(fetchObj.src, function(res) {
      fetchObj.status = 'downloading';
      fetchObj.progressEnd = parseInt(res.headers['content-length'], 10);
      res.pipe(file);
      res.on('data', function(chunk) {
        return fetchObj.progress += chunk.length;
      });
      return res.on('end', function() {
        return decompressDocs(docRoot, fileName, targetDir, fetchObj);
      });
    });
  };

  localZip = function(docRoot, targetDir, fetchObj) {
    return decompressDocs(docRoot, fetchObj.src, targetDir, fetchObj);
  };

  remoteDirectory = function(docRoot, targetDir, fetchObj) {
    var process;

    process = spawn('wget', ['-mk', '-P', "" + targetDir + "/", '--no-parent', fetchObj.src]);
    fetchObj.status = 'downloading';
    fetchObj.progress = 50;
    fetchObj.progressEnd = 100;
    process.stderr.on('data', function(data) {
      return print(data.toString());
    });
    process.stdout.on('data', function(data) {
      return print(data.toString());
    });
    return process.on('exit', function(code) {
      fetchObj.status = 'registering';
      registry.registerDoc(docRoot, fetchObj);
      return finishJob(fetchObj);
    });
  };

  localDirectory = function(docRoot, targetDir, fetchObj) {
    fetchObj.status = 'copying';
    fetchObj.progress = 50;
    fetchObj.progressEnd = 100;
    return ncp(fetchObj.src, targetDir, {
      clobber: true
    }, function(err) {
      if (err) {
        console.log(err);
      }
      fetchObj.status = 'registering';
      registry.registerDoc(docRoot, fetchObj);
      return finishJob(fetchObj);
    });
  };

  decompressDocs = function(docRoot, file, targetDir, fetchObj) {
    var extract, zip;

    fetchObj.status = 'decompressing';
    if (file.match(/\.zip$/)) {
      zip = new AdmZip(file);
      zip.extractAllTo(targetDir, true);
      fetchObj.status = 'registering';
      registry.registerDoc(docRoot, fetchObj);
      return finishJob(fetchObj);
    } else if (file.match(/\.tar\.gz$/)) {
      return extract = new targz().extract(file, targetDir, function(err) {
        if (err) {
          console.log(err);
        }
        fetchObj.status = 'registering';
        registry.registerDoc(docRoot, fetchObj);
        return finishJob(fetchObj);
      });
    }
  };

  finishJob = function(fetchObj) {
    var index;

    index = fetchState.stack.indexOf(fetchObj);
    if (index !== -1) {
      fetchState.stack.splice(index, 1);
    }
    if (fetchState.stack.length < 1) {
      return fetchState.working = false;
    }
  };

}).call(this);
