(function() {
  var app, docServerBar, homeCtrl, readCtrl;

  app = angular.module('docserver', []);

  app.config([
    '$routeProvider', function($routeProvider) {
      return $routeProvider.when('/', {
        templateUrl: 'partials/home.html',
        controller: 'homeCtrl'
      }).when('/read/*readPath', {
        templateUrl: 'partials/read.html',
        controller: 'readCtrl'
      }).otherwise({
        redirectTo: '/'
      });
    }
  ]);

  homeCtrl = function($scope, api) {
    return $scope.deleteDocs = function(lang, lib, version) {
      console.log('deleting');
      return api.deleteDoc(lang, lib, version);
    };
  };

  homeCtrl.$inject = ['$scope', 'api'];

  app.controller('homeCtrl', homeCtrl);

  readCtrl = function($scope, $route, $routeParams, $location) {
    var lastRoute;

    lastRoute = $route.current;
    $scope.$on('$locationChangeSuccess', function(event) {
      if ($route.current.$$route.controller === 'readCtrl') {
        if ($location.forceRouteChange) {
          return delete $location.forceRouteChange;
        } else {
          return $route.current = lastRoute;
        }
      }
    });
    document.docPageChange = function() {
      var docsActiveURL, match;

      if (this.activeElement.contentWindow) {
        match = this.activeElement.contentWindow.location.href.match(/\/docs\/(.+)$/);
        if (match) {
          docsActiveURL = match[1];
          $location.url("/read/" + docsActiveURL);
          return $scope.$apply();
        }
      }
    };
    return $scope.pathToLoad = "/docs/" + $routeParams.readPath;
  };

  readCtrl.$inject = ['$scope', '$route', '$routeParams', '$location'];

  app.controller('readCtrl', readCtrl);

  docServerBar = function($scope, $route, $routeParams, $location, api) {
    $scope.location = $location;
    $scope.$on('$locationChangeSuccess', function(event) {
      var params;

      params = $location.path().split('/');
      return $scope.current = {
        lang: params[2],
        lib: params[3],
        version: params[4]
      };
    });
    $scope.toggleAddingDoc = function() {
      if ($scope.newDoc) {
        return delete $scope.newDoc;
      } else {
        return $scope.newDoc = {
          lang: '',
          lib: '',
          version: '',
          src: ''
        };
      }
    };
    $scope.assumption = function(srcPath) {
      var location, retrieveAs;

      if (!srcPath) {
        return '';
      }
      location = '';
      retrieveAs = '';
      if (srcPath.match(/^(http|https|ftp)/)) {
        location = 'remote download of';
      } else {
        location = 'local transfer of';
      }
      if (srcPath.match(/\.(zip|tar\.gz|7z)$/)) {
        retrieveAs = 'zipped file';
      } else if (srcPath.match(/\.(html)/)) {
        retrieveAs = 'single html file';
      } else {
        retrieveAs = 'directory';
      }
      if (retrieveAs === 'single html file') {
        return "NOT YET SUPPORTED *** " + location + " a " + retrieveAs;
      }
      if (retrieveAs === 'zipped file' && location === 'local transfer of') {
        return "NOT YET SUPPORTED *** " + location + " a " + retrieveAs;
      }
      return "" + location + " a " + retrieveAs;
    };
    $scope.getAltLangs = function(currentLangName) {
      var langs;

      if (currentLangName) {
        langs = $.grep($scope.docRegistry.langs, function(l) {
          return l.name !== currentLangName;
        });
        return langs;
      } else {
        return [];
      }
    };
    $scope.getAltLibs = function(langName, currentLibName) {
      var lang, libs;

      if (langName && currentLibName) {
        lang = $.grep($scope.docRegistry.langs, function(l) {
          return l.name === langName;
        })[0];
        if (lang) {
          libs = $.grep(lang.libs, function(l) {
            return l.name !== currentLibName;
          });
          return libs;
        } else {
          return [];
        }
      } else {
        return [];
      }
    };
    $scope.getAltVersions = function(langName, libName, currentVersionName) {
      var lang, lib, versions;

      if (langName && libName && currentVersionName) {
        lang = $.grep($scope.docRegistry.langs, function(l) {
          return l.name === langName;
        })[0];
        if (lang) {
          lib = $.grep(lang.libs, function(l) {
            return l.name === libName;
          })[0];
          if (lib) {
            versions = $.grep(lib.versions, function(l) {
              return l.name !== currentVersionName;
            });
            return versions;
          } else {
            return [];
          }
        } else {
          return [];
        }
      } else {
        return [];
      }
    };
    $scope.changeReadURL = function(newURL) {
      $location.forceRouteChange = true;
      return $location.path(newURL);
    };
    return $scope.startDownloading = function() {
      return api.startFetch($scope.newDoc);
    };
  };

  docServerBar.$inject = ['$scope', '$route', '$routeParams', '$location', 'api'];

  app.controller('docServerBar', docServerBar);

  app.directive('progressBar', function() {
    return {
      restrict: "EAC",
      link: function($scope, elem, attrs) {
        var updateBar;

        updateBar = function() {
          var decimal, totalWidth;

          if (attrs.limit && attrs.progress) {
            decimal = $scope.$eval(attrs.progress) / $scope.$eval(attrs.limit);
            if (decimal === NaN) {
              decimal = 0.0;
            }
          } else {
            decimal = 0.0;
          }
          if ($(elem).find('.progress').length < 1) {
            $(elem).append("<div class='progress'></div>");
          }
          decimal = $scope.$eval(attrs.progress) / $scope.$eval(attrs.limit);
          totalWidth = $(elem).width();
          $(elem).find('.progress').animate({
            width: totalWidth * decimal
          }, 1000);
          return $scope.percentage = decimal * 100;
        };
        $scope.$watch(attrs.progress, updateBar);
        $scope.$watch(attrs.limit, updateBar);
        return updateBar();
      }
    };
  });

  app.filter('sortVersions', function() {
    return function(versions) {
      if (versions) {
        return versions.sort(function(a, b) {
          if (a.name === b.name) {
            return 0;
          }
          if (a.name === 'latest' && b.name !== 'latest') {
            return -1;
          }
          if (a.name !== 'latest' && b.name === 'latest') {
            return 1;
          }
          if (a.name === 'current' && b.name !== 'current') {
            return -1;
          }
          if (a.name !== 'current' && b.name === 'current') {
            return 1;
          }
          if (a.name > b.name) {
            return -1;
          }
          if (a.name < b.name) {
            return 1;
          }
          return 0;
        });
      }
      return versions;
    };
  });

  app.factory('api', [
    '$http', '$rootScope', '$timeout', function($http, $rootScope, $timeout) {
      var apiObj, concatStackId, docRegistry, fetchState, updateFetchState;

      fetchState = {
        working: false,
        stack: []
      };
      docRegistry = {
        langs: []
      };
      $rootScope.fetchState = fetchState;
      $rootScope.docRegistry = docRegistry;
      concatStackId = function(item) {
        return item.lang + item.lib + item.version;
      };
      updateFetchState = function(newData) {
        var index, newJobs, toRemove, _i, _len;

        toRemove = [];
        $.each(fetchState.stack, function(index, localItem) {
          var match;

          localItem.updated = false;
          match = $.grep(newData.stack, function(serverItem, index2) {
            return concatStackId(serverItem) === concatStackId(localItem);
          });
          if (match.length === 1) {
            angular.extend(localItem, match[0]);
            match[0].pushed = true;
            return newData.stack.splice(newData.stack.indexOf(match[0]), 1);
          } else {
            return toRemove.push(index);
          }
        });
        for (_i = 0, _len = toRemove.length; _i < _len; _i++) {
          index = toRemove[_i];
          fetchState.stack.splice(index, 1);
        }
        newJobs = $.grep(newData.stack, function(serverItem, index2) {
          if (serverItem.pushed) {
            return false;
          } else {
            return true;
          }
        });
        fetchState.stack = fetchState.stack.concat(newJobs);
        return fetchState.working = newData.working;
      };
      apiObj = {
        pollRegistry: function() {
          var req;

          req = $http.get('/api/registry_state');
          return req.success(function(data, status, headers) {
            return angular.extend(docRegistry, data);
          });
        },
        startFetch: function(newDoc) {
          var req;

          req = $http.post('/api/start_fetch', {
            lang: newDoc.lang,
            lib: newDoc.lib,
            version: newDoc.version,
            src: newDoc.src
          });
          return req.success(function(data, status, headers) {
            updateFetchState(data);
            return apiObj.pollFetchState();
          });
        },
        deleteDoc: function(lang, lib, version) {
          var postData, req;

          postData = {};
          if (lang) {
            postData.lang = lang;
          }
          if (lib) {
            postData.lib = lib;
          }
          if (version) {
            postData.version = version;
          }
          req = $http.post('/api/delete_doc', postData);
          return req.success(function(data, status, headers) {
            return angular.extend(docRegistry, data);
          });
        },
        pollFetchState: function() {
          var req;

          req = $http.get('/api/work_state');
          return req.success(function(data, status, headers) {
            if (fetchState.stack.length !== data.stack.length) {
              apiObj.pollRegistry();
            }
            updateFetchState(data);
            if (fetchState.working) {
              return $timeout(apiObj.pollFetchState, 1000);
            }
          });
        }
      };
      apiObj.pollFetchState();
      apiObj.pollRegistry();
      return apiObj;
    }
  ]);

}).call(this);
