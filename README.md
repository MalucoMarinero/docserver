# DocServer 0.1.1

** This is barely out of the oven, provided as is with ZERO guarantees, MIT license **

Offline Docs are better, they're faster and you don't have to deal with w3fools.com et al polluting your search results, but managing them can be a pain in the behind. Here's where DocServer comes in, tell it where the docs at and it'll keep it organised by:

- programming language
- library name
- version

At present DocServer can use the following sources:

- fetch an online .zip file
- mirror an online directory using wget as the backend
- copy from a local directory

## Installation
Install Node

`npm install -g node-docserver`

Run `nodeDocServer`, it should be on your path.

Point your browser at `http://localhost:8089`

Go Hog Wild!

## Usage
Click the top right button to start downloading a new doc. Enter the details of your docs and then click to start. Here's some examples:

Language: Python  
Library: Django  
Version: 1.5  
Source: https://docs.djangoproject.com/s/docs/django-docs-1.5-en.zip  

Language: Python  
Library: StdLib  
Version: 2.7.4  
Source: http://docs.python.org/ftp/python/doc/2.7.4/python-2.7.4-docs-html.zip  

Language: Coffeescript  
Library: Reference  
Version: latest  
Source: http://coffeescript.org  

Language: Javascript  
Library: NodeJS  
Version: latest  
Source: http://nodejs.org/api/  

## You break it, you bought it.
This is brand new, excessively so. All file operations are done without admin privileges, and when used as intended will only operate within ~/.node-docserver. If you mess up your install, just delete that folder and you can start again. Currently the app is limited to localhost only as there isn't a shred of input validation going on.

Only used in Ubuntu, send me trip reports on other platforms!


## Changes 0.1.1
- Quick Switcher implemented.
- Docs can now be deleted from home page (deleting them in ~/.node-docserver as well)
- Version directories no longer prefixed with 'v'.
- tar.gz file support added.



Copyright (c) 2013 Full and By Design
MIT License
